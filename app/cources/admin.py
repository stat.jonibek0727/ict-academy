from django.contrib import admin
from .models import CourceCategory, Cources
from modeltranslation.admin import TranslationAdmin
# Register your models here.

class CoursesAdmin(TranslationAdmin):
    list_display = ('title', 'category','description')
    list_filter = ('title', 'description')
    search_fields = ('title', 'category','description')
    
admin.site.register(CourceCategory, TranslationAdmin)
admin.site.register(Cources, CoursesAdmin)