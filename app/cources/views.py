from django.shortcuts import render
from .models import CourceCategory, Cources
from django.views import View
# Create your views here.

class CourcesList(View):
    def get(self, request):
        cource_category = CourceCategory.objects.all()
        context = {
            'cource_category' : cource_category
        }
        return render(request, 'courses.html', context)
    
class CourceCategpry(View):
    def get(self, request, id):
        cources = Cources.objects.filter(category__id = id)
        context = {
            "cources" : cources
        }
        return render(request, 'course-category.html', context)
    
class CourceDetail(View):
    def get(self, request, id):
        cource_detail = Cources.objects.get(id = id)
        cources = Cources.objects.all()
        context = {
            "cource_detail" : cource_detail,
            "cources" : cources
        }
        return render(request, 'course-details.html', context)
        