from django.db import models
from app.base.models import BaseModel
from ckeditor_uploader.fields import RichTextUploadingField
from app.home.models import Mentors
# Create your models here.

CATEGORY_STTYLE = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)




class CourceCategory(BaseModel):
    name = models.CharField(max_length = 255, null=True, blank = True)
    icon = models.ImageField(upload_to='court_categry/', null=True, blank = True)
    style = models.CharField(max_length = 1, null=True, blank = True, choices = CATEGORY_STTYLE)
    
    def __str__(self):
        return self.name
    
class Cources(BaseModel):
    title = models.CharField(max_length = 255, null=True, blank = True)
    description = models.TextField(null=True, blank = True)
    photo = models.ImageField(upload_to='cource_photo/', null=True, blank = True)
    category = models.ForeignKey(CourceCategory, on_delete=models.SET_NULL, null=True, blank=True, related_name='cource')
    
    overview = RichTextUploadingField()
    curriculum = RichTextUploadingField()
    teacher = models.ForeignKey(Mentors, on_delete=models.SET_NULL, null=True, blank=True, related_name='cource')
    help_title = models.CharField(max_length = 255, null=True, blank = True)
    help_description = models.TextField(null=True, blank = True)
    help_photo = models.ImageField(upload_to='help_photo/')
    
    video_photo = models.ImageField(upload_to='video_photo/')
    video_link = models.URLField(null=True, blank = True)
    
    price = models.IntegerField(null=True, blank=True)
    descount = models.IntegerField(null=True, blank=True)
    delay_time = models.DateTimeField(null=True, blank=True)
    
    @property
    def get_descount_price(self):
        if self.descount:
            price_des = self.price - (self.price * self.descount)/100
            return price_des
        else:
            return None
    
    def __str__(self):
        return self.title