# Generated by Django 5.0.1 on 2024-01-09 19:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cources', '0003_cources'),
    ]

    operations = [
        migrations.AddField(
            model_name='cources',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to='cource_photo/'),
        ),
    ]
