# Generated by Django 5.0.1 on 2024-01-08 20:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cources', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='courcecategory',
            name='style',
            field=models.CharField(blank=True, choices=[('1', '1'), ('2', '2'), ('3', '3')], max_length=1, null=True),
        ),
    ]
