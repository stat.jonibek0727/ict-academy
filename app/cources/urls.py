from django.urls import path
from .views import CourcesList, CourceCategpry, CourceDetail

app_name = 'cource'
urlpatterns = [
    path('cource-list/', CourcesList.as_view(), name='cource_list'),
    path('cource-cateory/<uuid:id>/', CourceCategpry.as_view(), name='cource_category'),
    path('cource/<uuid:id>/', CourceDetail.as_view(), name='cource_detail'),
]
