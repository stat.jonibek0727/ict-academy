from modeltranslation.translator import translator, TranslationOptions, register
from .models import (
    CourceCategory,
    Cources,
    
)

@register(CourceCategory)
class CourceCategoryTranslationOptions(TranslationOptions):
    fields = (
        'name',
    )
    
@register(Cources)
class CourcesTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'description',
        'overview',
        'curriculum',
        'help_title',
        'help_description',
    )