from django.db import models
from django.contrib.auth.models import AbstractUser
from app.cources.models import Cources
# Create your models here.

class User(AbstractUser):
    phone = models.CharField(max_length=50, null=True, blank=True)
    photo = models.ImageField(upload_to='profile/', default='default/1.png')
    cource = models.ForeignKey(Cources, on_delete=models.SET_NULL, null=True, blank=True, related_name='user')

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.username
