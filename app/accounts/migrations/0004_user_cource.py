# Generated by Django 5.0.1 on 2024-01-09 19:18

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_alter_user_photo'),
        ('cources', '0003_cources'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='cource',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user', to='cources.cources'),
        ),
    ]
