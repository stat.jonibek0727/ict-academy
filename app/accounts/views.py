from django.shortcuts import render, redirect
from django.views import View
from .forms import UserCreateForm, CustomAuthForm
from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


class SignUpView(View):
    form_class = UserCreateForm
    
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home:home')
        create_form = self.form_class
        context = {
            "form": create_form
        }
        return render(request, "signup.html", context)

    def post(self, request):
        create_form = self.form_class(data=request.POST)
        if create_form.is_valid():
            create_form.save()
            return redirect('accounts:signin')
        else:
            messages.error(request, f"{create_form.errors}")
            context = {
                "form": create_form
            }
            return render(request, "signup.html", context)



class LoginView(View):
    form_class = CustomAuthForm
    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home:home')
        else:
            login_form = self.form_class

            return render(request, "signin.html", {"login_form": login_form})

    def post(self, request):
     
        
        login_form = self.form_class(data=request.POST)

        if login_form.is_valid():
            user = login_form.get_user()
            login(request, user)

            messages.success(request, "You have successfully logged in.")

            return redirect('home:home')
        else:
            return render(request, "signin.html", {"login_form": login_form})


class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        messages.info(request, "You have successfully logged out.")
        return redirect("home:home")
