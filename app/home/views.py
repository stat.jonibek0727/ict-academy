from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from .models import HomePage, Mentors, ClientMind, Companies, AboutUs, Project
from app.blog.models import Blog
from app.cources.models import CourceCategory, Cources
from django.core.paginator import Paginator
from .forms import ContactForm
# Create your views here.

class HomePageView(View):
    
    def get(self, request, *args, **kwargs):
        homepage = HomePage.objects.first()
        category_cource = CourceCategory.objects.all()
        mentors = Mentors.objects.all()[:6]
        client_mind = ClientMind.objects.all()[:5]
        blog = Blog.objects.all()[:3]
        companies = Companies.objects.all()
        cources = Cources.objects.all().order_by('-create_at')
        context = {
            'homepage' : homepage,
            'category_cource' : category_cource,
            'mentors' : mentors,
            'client_mind' : client_mind,
            'blog' : blog,
            'companies' : companies,
            'cources' : cources
        }
        
        return render(request, 'index.html', context)


class AboutUsView(View):
    def get(self, request, *args, **kwargs):
        mentors = Mentors.objects.all()[:6]
        homepage = HomePage.objects.first()
        client_mind = ClientMind.objects.all()[:5]
        about = AboutUs.objects.last()
        project = Project.objects.all()
        cources = Cources.objects.all().order_by('-create_at')
        companies = Companies.objects.all()
        
        
        context = {
            'homepage' : homepage,
            'mentors' : mentors,
            'client_mind' : client_mind,
            'about' : about,
            'project' : project,
            'cources' : cources,
            'companies' : companies
        }
        
        return render(request, 'about.html', context)
        
        
class MentorsView(View):
    def get(self, request, *args, **kwargs):
        mentors = Mentors.objects.all()
        page_size = request.GET.get('page_size', 20)
        paginator = Paginator(mentors, page_size)

        page_num = request.GET.get('page', 1)
        page_obj = paginator.get_page(page_num)

        context = {
           'mentors' : page_obj 
        }
        
        return render(request, 'mentors.html', context)
    
    
class MentorDetailView(View):
    def get(self, request, id, *args, **kwargs):
        mentor = get_object_or_404(Mentors, id=id)
        context = {
           'mentor' : mentor
        }
        return render(request, 'mentor-details.html', context)
    
    
class ContactUsView(View):
    form = ContactForm
    def get(self, request, *args, **kwargs):
        
        form_class = self.form
        context = {
           'form_class' : form_class
        }
        return render(request, 'contact.html', context)
    
    def post(self, request):
        url = request.META.get('HTTP_REFERER')
        create_form = self.form(data=request.POST)
        if create_form.is_valid():
            create_form.save()
            return redirect(url)
        else:
           
            context = {
                "form": create_form
            }
            return render(request, "contact.html", context)