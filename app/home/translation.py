from modeltranslation.translator import translator, TranslationOptions, register

from .models import (
    HomePage,
    Mentors,
    ClientMind,
    Project,
    AboutUs,
)


@register(HomePage)
class HomePageTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'description',
        'category_description',
        'about_description',
        'cource_description',
        'mentor_description',
        'community',
        'story_description',
    )
    
@register(Mentors)
class MentorsTranslationOptions(TranslationOptions):
    fields = (
        'full_name',
        'specialty',
        'description',
    )
    

@register(ClientMind)
class ClientMindTranslationOptions(TranslationOptions):
    fields = (
        'body',
        'full_name',
        'specialty',
    )
    

@register(AboutUs)
class AboutUsTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'body',
        'project_title',
        'project_body',
    )
    
    
@register(Project)
class ProjectTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'body',
    )

