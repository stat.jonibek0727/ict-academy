from django.urls import path
from app.home import views

app_name = "home"
urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('about/', views.AboutUsView.as_view(), name='about'),
    path('mentor/', views.MentorsView.as_view(), name='mentor'),
    path('mentor/<uuid:id>', views.MentorDetailView.as_view(), name='mentor_detail'),
    path('contact/', views.ContactUsView.as_view(), name='contact'),
    
]
