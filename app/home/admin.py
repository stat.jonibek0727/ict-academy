from django.contrib import admin
from .models import HomePage, ClientMind, Mentors, Companies, Project, AboutUs, ContactUs
from modeltranslation.admin import TranslationAdmin
# Register your models here.

class HomePageAdmin(TranslationAdmin):
    list_display = ('title', 'description')
    
class MentorsAdmin(TranslationAdmin):
    list_display = ('full_name', 'specialty')
    search_fields = ('full_name', 'specialty')
    
class ProjectAdmin(TranslationAdmin):
    list_display = ('title', 'body')
    search_fields = ('title', 'body')
    list_filter = ('title', 'body')
    
class ContactAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'phone', 'create_at')
    search_fields = ('full_name', 'phone')
    list_filter = ('full_name', 'phone')
    
    
admin.site.register(HomePage, HomePageAdmin)
admin.site.register(ClientMind, TranslationAdmin)
admin.site.register(Mentors, MentorsAdmin)
admin.site.register(Companies)
admin.site.register(AboutUs, TranslationAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ContactUs, ContactAdmin)