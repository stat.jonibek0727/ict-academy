from django.contrib import admin
from .models import Blog, BlogCategories, BlogComment, BlogTags
from modeltranslation.admin import TranslationAdmin
# Register your models here.

class BlogAdmin(TranslationAdmin):
    list_display = ('title', 'category','body', 'user')
    list_filter = ('title', 'body', 'user')
    search_fields = ('title', 'category','body', 'user')

admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogCategories, TranslationAdmin)
admin.site.register(BlogComment, TranslationAdmin)
admin.site.register(BlogTags)