from modeltranslation.translator import translator, TranslationOptions, register
from .models import (
    BlogCategories,
    Blog,
    BlogComment,
)


@register(BlogCategories)
class BlogCategoriesTranslationOptions(TranslationOptions):
    fields = (
        'name',
    )
    
@register(Blog)
class BlogTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'body',
    )
    
@register(BlogComment)
class BlogCommentTranslationOptions(TranslationOptions):
    fields = (
        'comment',
    )