# Generated by Django 5.0.1 on 2024-01-08 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_blog_style'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='photo1',
            field=models.ImageField(default=1, upload_to='blog/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='blog',
            name='photo2',
            field=models.ImageField(default=1, upload_to='blog/'),
            preserve_default=False,
        ),
    ]
