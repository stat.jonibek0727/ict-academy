from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    path('admin-sevara/', admin.site.urls),
    path('rosetta/', include('rosetta.urls')), 
    path('social-auth/', include('social_django.urls', namespace='social')),
    path('auth/', include('app.accounts.urls')),
    ]

urlpatterns += i18n_patterns(
    path('', include('app.home.urls')),
    path('blog/', include('app.blog.urls')),
    path('cource/', include('app.cources.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
  
)

# if settings.DEBUG:
urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
